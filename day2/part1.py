import os

with open(os.path.join(os.path.dirname(__file__), "input.txt")) as input:
    moves = [x.strip('\n').split(' ') for x in input.readlines()]

position = [0,0]
for move in moves:
    if move[0] == "up":
        position[1] -= int(move[1])
    elif move[0] == "down":
        position[1] += int(move[1])
    else:
        position[0] += int(move[1])

result = position[0] * position[1]
print(result)

# one liner
print(sum([int(x[1]) for x in moves if x[0] in ['forward']]) * sum([(int(x[1]) * -1 if x[0] == 'up' else int(x[1])) for x in moves if x[0] in ['down', 'up']]))
