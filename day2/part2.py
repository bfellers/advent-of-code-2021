import os

with open(os.path.join(os.path.dirname(__file__), "input.txt")) as input:
    moves = [x.strip('\n').split(' ') for x in input.readlines()]

position = [0,0]
aim = 0
for move in moves:
    if move[0] == "up":
        aim -= int(move[1])
    elif move[0] == "down":
        aim += int(move[1])
    else:
        position[0] += int(move[1])
        position[1] += int(move[1]) * aim

result = position[0] * position[1]
print(result)
