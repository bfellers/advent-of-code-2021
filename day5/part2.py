import os

with open(os.path.join(os.path.dirname(__file__), "input.txt")) as input:
    lines = [x.strip('\n') for x in input.readlines()]

def process(lines):
    # how big should grid be?
    numbers = []
    for x in lines:
        y = x.split(' -> ')
        a = y[0].split(',')
        b = y[1].split(',')
        numbers.append(int(a[0]))
        numbers.append(int(a[1]))
        numbers.append(int(b[0]))
        numbers.append(int(b[1]))
    
    grid_size = max(numbers) + 1

    # fill a grid with zeros
    grid = [[0 for x in range(grid_size)] for y in range(grid_size)]

    # place the lines
    for line in lines:
        (start, end) = line.split(' -> ')
        (start_x, start_y) = [int(x) for x in start.split(',')]
        (end_x, end_y) = [int(x) for x in end.split(',')]
        if start_x == end_x:
            (start_x, end_x) = sorted([start_x, end_x])
            (start_y, end_y) = sorted([start_y, end_y])
            for i in range(start_y, end_y + 1):
                grid[start_x][i] = grid[start_x][i] + 1
        elif start_y == end_y:
            (start_x, end_x) = sorted([start_x, end_x])
            (start_y, end_y) = sorted([start_y, end_y])
            for i in range(start_x, end_x + 1):
                grid[i][start_y] = grid[i][start_y] + 1
        else:
            x = start_x
            y = start_y
            x_adj = -1 if start_x > end_x else 1
            y_adj = -1 if start_y > end_y else 1
            while x != end_x and y != end_y:
                grid[x][y] = grid[x][y] + 1
                x += x_adj
                y += y_adj
            # hack because i dont want to refactor above,
            # we skip end_x and end_y so manually add it here
            grid[end_x][end_y] = grid[end_x][end_y] + 1
    
    # find number of intersections
    return sum([1 for x in grid for y in x if y > 1])

print(process(lines))