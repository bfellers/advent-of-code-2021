import os, math, uuid

class Board():
    ## pass in a list of strings representing the board
    def __init__(self, items):
        self.__called_numbers = []
        self.__contents = []
        self.id = uuid.uuid4()
        self.winning_score = None
        for x in items:
            l = []
            for y in x.split(' '):
                if y:
                    l.append(int(y))
            self.__contents.append(l)

    def __repr__(self):
        ret = '=== Board ===\n'
        for x in self.__contents:
            for y in x:
                if y in self.__called_numbers:
                    ret += f'|##'
                else:
                    ret += f'|{y:2}'
            ret += '|\n'
        ret += '\n'
        return ret
    
    def call_number(self, number):
        self.__called_numbers.append(number)
    
    def get_score(self):
        score = 0
        for x in self.__contents:
            for y in x:
                if y not in self.__called_numbers:
                    score += y
        return score
    
    def is_winner(self):
        # check x/y
        for x in range(5):
            if set([
                self.__contents[x][0], 
                self.__contents[x][1], 
                self.__contents[x][2], 
                self.__contents[x][3], 
                self.__contents[x][4], 
            ]).issubset(self.__called_numbers):
                return self.get_score()
        for y in range(5):
            if set([
                self.__contents[0][y], 
                self.__contents[1][y], 
                self.__contents[2][y], 
                self.__contents[3][y], 
                self.__contents[4][y], 
            ]).issubset(self.__called_numbers):
                return self.get_score()
        if set([
                    self.__contents[0][0], 
                    self.__contents[1][1], 
                    self.__contents[2][2], 
                    self.__contents[3][3], 
                    self.__contents[4][4], 
                ]).issubset(self.__called_numbers):
                    return self.get_score()
        if set([
                    self.__contents[4][0], 
                    self.__contents[3][1], 
                    self.__contents[2][2], 
                    self.__contents[1][3], 
                    self.__contents[0][4], 
                ]).issubset(self.__called_numbers):
                    return self.get_score()


with open(os.path.join(os.path.dirname(__file__), "input.txt")) as input:
    lines = [x.strip('\n') for x in input.readlines()]
    lines = [x for x in lines if x]

called_numbers = lines.pop(0)

boards = []

# Build Boards
x = 0
while x < len(lines):
    boards.append(Board(lines[x:x+5]))
    x += 5


def find_last():
    winners = []
    for called in [int(x) for x in called_numbers.split(',')]:
        for board in boards:
            board.call_number(called)
            if board.is_winner() and board.id not in [x.id for x in winners]:
                board.winning_score = (board.get_score() * called)
                winners.append(board)
            if len(winners) == len(boards):
                return board.winning_score

print(find_last())
