import os, math

class Board():
    ## pass in a list of strings representing the board
    def __init__(self, items):
        self.__called_numbers = []
        self.__contents = []
        for x in items:
            l = []
            for y in x.split(' '):
                if y:
                    l.append(int(y))
            self.__contents.append(l)
        self.__potential_lines = []

    def __repr__(self):
        ret = '=== Board ===\n'
        for x in self.__contents:
            for y in x:
                if y in self.__called_numbers:
                    ret += f'|##'
                else:
                    ret += f'|{y:2}'
            ret += '|\n'
        ret += '\n'
        return ret
    
    def call_number(self, number):
        self.__called_numbers.append(number)
    
    def get_score(self):
        score = 0
        for x in self.__contents:
            for y in x:
                if y not in self.__called_numbers:
                    score += y
        return score
    
    def is_winner(self):
        for line in self.potential_lines():
            if set(line).issubset(self.__called_numbers):
                return True
    
    def potential_lines(self):
        if self.__potential_lines:
            return self.__potential_lines
        else:
            for x in range(5):
                self.__potential_lines.append([
                    self.__contents[x][0], 
                    self.__contents[x][1], 
                    self.__contents[x][2], 
                    self.__contents[x][3], 
                    self.__contents[x][4], 
                ])
            for y in range(5):
                self.__potential_lines.append([
                    self.__contents[0][y], 
                    self.__contents[1][y], 
                    self.__contents[2][y], 
                    self.__contents[3][y], 
                    self.__contents[4][y], 
                ])
            self.__potential_lines.append([
                        self.__contents[0][0], 
                        self.__contents[1][1], 
                        self.__contents[2][2], 
                        self.__contents[3][3], 
                        self.__contents[4][4], 
                    ])
            self.__potential_lines.append([
                        self.__contents[4][0], 
                        self.__contents[3][1], 
                        self.__contents[2][2], 
                        self.__contents[1][3], 
                        self.__contents[0][4], 
                    ])
            return self.__potential_lines


with open(os.path.join(os.path.dirname(__file__), "input.txt")) as input:
    lines = [x.strip('\n') for x in input.readlines()]
    lines = [x for x in lines if x]

called_numbers = lines.pop(0)

boards = []

# Build Boards
x = 0
while x < len(lines):
    boards.append(Board(lines[x:x+5]))
    x += 5


def play_game():
    for called in [int(x) for x in called_numbers.split(',')]:
        for board in boards:
            board.call_number(called)
            if board.is_winner():
                return (board.get_score() * called)

#print(play_game())

import timeit
timeit.timeit('play_game()', 'from __main__ import play_game, Board', number=100)

