import os

with open(os.path.join(os.path.dirname(__file__), "input.txt")) as input:
    items = [int(x.strip('\n')) for x in input.readlines()]

increases = len([x for x in range(0, len(items) - 1) if items[x] < items[x + 1]])

print(increases)

# reddit oneliner
increases = sum(map(int.__gt__, items[1:], items))

print(increases)