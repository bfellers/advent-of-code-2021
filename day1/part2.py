import os

with open(os.path.join(os.path.dirname(__file__), "input.txt")) as input:
    items = [int(x.strip('\n')) for x in input.readlines()]

increases = len([x for x in range(len(items) - 3) if sum(items[x:x+3]) < sum(items[x+1:x+4])])

print(increases)

# reddit oneliner
increases = sum(map(int.__gt__, items[3:], items))

print(increases)