import os

with open(os.path.join(os.path.dirname(__file__), "input.txt")) as input:
    fish = [int(x) for x in input.read().strip('\n').split(',')]

def process(fish, days):
    for day in range(days):
        for f in range(len(fish)):
            if fish[f] == 0:
                fish[f] = 6
                fish.append(8)
            else:
                fish[f] = fish[f] - 1
    return len(fish)

print(process(fish, 80))