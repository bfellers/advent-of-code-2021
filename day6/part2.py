import os

with open(os.path.join(os.path.dirname(__file__), "input.txt")) as input:
    fish = [int(x) for x in input.read().strip('\n').split(',')]

def process(fish, days):
    fish_dict = {x: 0 for x in range(9)}

    for f in fish:
        fish_dict[f] = fish_dict[f] + 1

    for day in range(days):
        print(day)
        temp = fish_dict.copy()
        to_add = 0
        reset = 0
        for k in sorted(fish_dict.keys()):
            if k == 0:
                to_add += fish_dict[k]
                reset += fish_dict[k]
            else:
                temp[k - 1] = fish_dict[k]
        temp[8] = to_add
        temp[6] = temp[6] + reset
        fish_dict = temp.copy()

    return sum([v for v in fish_dict.values()])

print(process(fish, 256))