import os, statistics

with open(os.path.join(os.path.dirname(__file__), "input.txt")) as input:
    crabs = [int(x) for x in input.read().strip('\n').split(',')]

dest = int(statistics.median(crabs))
cost = sum([abs(x - dest) for x in crabs])

print(cost)