import os

with open(os.path.join(os.path.dirname(__file__), "input.txt")) as input:
    crabs = [int(x) for x in input.read().strip('\n').split(',')]

cheapest = cheapest_dest = None
for i in range(min(crabs), max(crabs) + 1):
    crab_steps = [abs(x - i) for x in crabs]
    cost = sum([(x * (x + 1)) / 2 for x in crab_steps])
    if not cheapest or cost < cheapest:
        cheapest = cost
        cheapest_dest = i

print(int(cheapest))