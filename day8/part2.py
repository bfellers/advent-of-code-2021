import os

class InputOutput():
    def __init__(self, input):
        self.input = input.split('|')[0].split(' ')
        self.input.remove('')
        self.output = input.split('|')[1].split(' ')
        self.output.remove('')

    def __repr__(self):
        return f'input: {" ".join(self.input)} output: {" ".join(self.output)}'

    def counter(self):
        return len([x for x in self.output if len(x) < 5 or len(x) == 7])

with open(os.path.join(os.path.dirname(__file__), "input.txt")) as input:
    inputs = [InputOutput(x.strip('\n')) for x in input.readlines()]

print(sum([x.counter() for x in inputs]))
