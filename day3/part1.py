import os, math

with open(os.path.join(os.path.dirname(__file__), "input.txt")) as input:
    lines = [x.strip('\n') for x in input.readlines()]

gamma_binary = ''
for i in range(len(lines[0])):
    temp = [int(x[i]) for x in lines]
    gamma_binary += str(math.trunc(sum(temp) / len(temp) + .5))

epsilon_binary = ''.join(['1' if x == '0' else '0' for x in gamma_binary])
total = int(gamma_binary, 2) * int(epsilon_binary, 2)
print(total)
