import os, math

with open(os.path.join(os.path.dirname(__file__), "input.txt")) as input:
    lines = [x.strip('\n') for x in input.readlines()]

oxygen_generator = lines.copy()
co2_scrubber = lines.copy()

i = 0 
while len(oxygen_generator) > 1:
    temp = [int(x[i]) for x in oxygen_generator]
    oxygen_generator = [x for x in oxygen_generator if x[i] == str(math.trunc(sum(temp) / len(temp) + .5))]
    i+=1

i = 0 
while len(co2_scrubber) > 1:
    temp = [int(x[i]) for x in co2_scrubber]
    co2_scrubber = [x for x in co2_scrubber if x[i] != str(math.trunc(sum(temp) / len(temp) + .5))]
    i+=1

print(int(oxygen_generator[0], 2) * int(co2_scrubber[0], 2))